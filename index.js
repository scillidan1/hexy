const input = document.getElementById('equation');
const hexy = document.getElementById('hexy');
const buffer = document.createElement('canvas');
const ctx = hexy.getContext('2d');
const ctx2 = buffer.getContext('2d');
const SIZE = 500;
const TILESIZE = 10;
const sprites = makeSprites(TILESIZE);
const sample = '10 * (Math.cos(x) - i * Math.sin(y))';

let demo = loadFn(sample);
let cx = 0;
let cy = 0;
let i = 0;
let requestId;

hexy.width = hexy.height = SIZE;
buffer.width = buffer.height = SIZE;

function loadFn (equation) {
  return new Function('x', 'y', 'i', 'cx', 'cy', `return ${equation}`);
}

function updateURL (str) {
  window.history.replaceState(null, null, `?code=${btoa(str)}`);
}

const params = new URLSearchParams(window.location.search);
if (params.get('code')) {
  const code = atob(params.get('code'));
  demo = loadFn(code);
  input.value = code;
} else {
  input.value = sample;
  updateURL(sample);
}

function start () {
  if (!requestId) requestId = window.requestAnimationFrame(render);
}

function stop () {
  if (requestId) {
    window.cancelAnimationFrame(requestId);
    requestId = undefined;
  }
}

function id (n) {
  return Math.abs(n | 0) % 10;
}

function render () {
  requestId = undefined;
  i = (i + .01) % 60;
  for (let y = 0; y < SIZE; y += TILESIZE) {
    for (let x = 0; x < SIZE; x += TILESIZE) {
      const n = demo(x / TILESIZE, y / TILESIZE, i, cx, cy);
      ctx2.putImageData(sprites[id(n)], x, y);
    }
  }
  ctx.clearRect(0, 0, SIZE, SIZE);
  ctx.drawImage(buffer, 0, 0);
  start();
}

function getMousePos (hexy, { clientX, clientY }) {
  const { left, top } = hexy.getBoundingClientRect();
  return { x: clientX - left, y: clientY - top };
}

hexy.onmousemove = function (e) {
  const { x, y } = getMousePos(hexy, e);
  cx = x;
  cy = y;
}

input.onchange = function (e) {
  const value = this.value.trim();
  if (value === '') return;
  stop();
  i = 0;
  demo = loadFn(value);
  try {
    demo();
  } catch (err) {
    console.log(err);
    demo = loadFn(sample);
  }
  render();
  updateURL(value);
}

const warning = confirm('WARNING: may contain flashing/flickering images that may trigger seizures in people with photosensitive epilepsy');
if (warning) render();
