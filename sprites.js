function makeSprites (res) {

  const empty = () => 0;

  function left (c, x, y) {
    c.moveTo(x, y);
    c.lineTo(x, y + res);
  }

  function top (c, x, y) {
    c.moveTo(x, y);
    c.lineTo(x + res, y);
  }

  function right (c, x, y) {
    c.moveTo(x + res, y);
    c.lineTo(x + res, y + res);
  }

  function bottom (c, x, y) {
    c.moveTo(x, y + res);
    c.lineTo(x + res, y + res);
  }

  function backslash (c, x, y) {
    c.moveTo(x, y);
    c.lineTo(x + res, y + res);
  }

  function slash (c, x, y) {
    c.moveTo(x, y + res);
    c.lineTo(x + res, y);
  }

  function box (c, x, y) {
    top(c, x, y);
    right(c, x, y);
    bottom(c, x, y);
    left(c, x, y);
  }

  function ex (c, x, y) {
    backslash(c, x, y);
    slash(c, x, y);
  }

  function crate (c, x, y) {
    box(c, x, y);
    ex(ctx, x, y);
  }

  const moves = [
    empty, left, top, right, bottom,
    backslash, slash, ex, box, crate
  ];

  const sheet = document.createElement('canvas');
  const ctx = sheet.getContext('2d', { alpha: false });
  const l = moves.length;
  const sprites = [];

  sheet.width = l * res;
  sheet.height = res;
  ctx.lineWidth = 0.4;
  ctx.strokeStyle = 'white';

  for (let i = 0; i < l; i++) {
    ctx.beginPath();
    const v = i * res;
    moves[i](ctx, v, 0);
    ctx.stroke();
    sprites.push(ctx.getImageData(v, 0, res, res));
  }

  return sprites;
}
